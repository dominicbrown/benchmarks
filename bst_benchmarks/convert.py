#!/usr/bin/env python3

import csv
import sys
import json
import argparse
import statistics


def read_json(filename):
    try:
        with open(filename) as f:
            return json.load(f)
    except IOError:
        print("Failed to open file:", filename, file=sys.stderr)
        sys.exit(1)

def main():
    parser = argparse.ArgumentParser(
        description="Script to pretify buildstream benchmark results")
    parser.add_argument("json_file", metavar="json-file",
                        help="the benchmark results to load (in json format)")
    parser.add_argument("output", help="name of the csv file output")
    args = parser.parse_args()

    fields = ['Version', 'Total Time', 'Max RSS']

    bm_json = read_json(args.json_file)
    formatted_results = {}
    for test in bm_json['tests']:
        result_entries = []
        for result in test['results']:
            rss_measurements = [m['max-rss-kb'] for m in result['measurements']]
            total_measurements = [m['total-time'] for m in result['measurements']]
            result_entries.append({
                'Version': result['version'],
                'Total Time': statistics.mean(total_measurements),
                'Max RSS': statistics.mean(rss_measurements)
            })
        formatted_results[test['name']] = result_entries

    try:
        with open(args.output, 'w') as outf:
            dw = csv.DictWriter(outf, fields)
            for name, entries in formatted_results.items():
                dw.writerow({fields[0]: name})
                dw.writeheader()
                dw.writerows(entries)
                dw.writerow({})
    except IOError:
        print("Failed to create file:", filename, file=sys.stderr)
        sys.exit(1)

if __name__ == "__main__":
    main()
